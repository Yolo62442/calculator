package main

import (
	"context"
	"fmt"
	"gitlab.com/Yolo62442/Calculator/calculator/calculatorpb"
	"google.golang.org/grpc"
	"log"
	"net"
	"strconv"
)

type Server struct {
	calculatorpb.UnimplementedCalculatorServiceServer
}

func (s *Server) CalculatorPrimeNumberDecomposition(ctx context.Context, req *calculatorpb.CalculatorPrimeNumberDecompositionRequest) (*calculatorpb.CalculatorPrimeNumberDecompositionResponse, error) {
	fmt.Printf("Greet function was invoked with %v \n", req)

	number := int(req.GetCalc().GetNum())
	var str string
	for i := 2; number > i; i++ {
		for number%i == 0 {
			a := strconv.Itoa(int(i))
			str = str + a + ", "
			number = number / i
		}
	}
	if number > 2 {
		num := strconv.Itoa(number)
		str = str + num
	}

	res := &calculatorpb.CalculatorPrimeNumberDecompositionResponse{
		Nums: str,
	}

	return res, nil
}
func main() {
	l, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen:%v", err)
	}
	s := grpc.NewServer()
	calculatorpb.RegisterCalculatorServiceServer(s, &Server{})
	log.Println("Server is running on port:50051")
	if err := s.Serve(l); err != nil {
		log.Fatalf("failed to serve:%v", err)
	}
}

