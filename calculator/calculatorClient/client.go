package main

import (
	"gitlab.com/Yolo62442/Calculator/calculator/calculatorpb"
	"context"
	"google.golang.org/grpc"
	"log"
)

func main() {

	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer conn.Close()

	c := calculatorpb.NewCalculatorServiceClient(conn)
	doUnary(c)
}

func doUnary(c calculatorpb.CalculatorServiceClient) {
	ctx := context.Background()

	request := &calculatorpb.CalculatorPrimeNumberDecompositionRequest{Calc: &calculatorpb.Calculating{
		Num: 120,
	}}

	response, err := c.CalculatorPrimeNumberDecomposition(ctx, request)
	if err != nil {
		log.Fatalf("error while calling Greet RPC %v", err)
	}
	log.Printf("response from Greet:%v", response.Nums)
}
